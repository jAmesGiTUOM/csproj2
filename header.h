#include <sys/resource.h>
#define WORDSIZE 4
#define CONNECTION 0
#define FEEDBACK 1
#define GAMEOVER_S 2
#define GAMEOVER_F 3
struct gameData{
          char secretWord[5];
          int client_fd;
          struct sockaddr_in server;
          struct sockaddr_in client;
          FILE* fp;

};

struct usageInfo {  
  int id;
  int runningClient;
  struct rusage start;
  struct rusage end;
};

pthread_mutex_t lock;
struct usageInfo usage[20];
struct rusage start;
struct rusage end;
int numOfClient;
int numOfWinner;
FILE *fp;


void* game_handler(void* client_num);
int correctPlacedChar(char* answer,char* secretWord);
int misplacedChar(char* answer,char* secretWord);
int positionChecked(int* list,int element,int num);
int checkValid(char c);
int checkInputLength(char* word);
void  INThandler(int sig);


void generateLogFile(FILE *fp,int client_fd,struct sockaddr_in client,struct sockaddr_in server,
char* secretWord,char* clientGuess,char* hint,int flag);
char* generateSecretWord();
void printIP(FILE* fp,struct sockaddr_in client);
void printTime(FILE* fp,int flag);
void printStats();

