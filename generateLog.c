#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include "header.h"

void generateLogFile(FILE *fp,int client_fd,struct sockaddr_in client,struct sockaddr_in server,char* secretWord,char* clientGuess,char* hint,int flag)
{
    fp=fopen("log.txt","a");

    if(flag==CONNECTION){
        printTime(fp,flag);
        printIP(fp,client);
        fprintf(fp,"(%d)",client_fd);
        fprintf(fp, " client connected\n");

        printTime(fp,flag);
        printIP(fp,server);
        fprintf(fp, " server secret=%s\n",secretWord);
    }
    if(flag==FEEDBACK)
    {
        printTime(fp,flag);
        printIP(fp,client);
        fprintf(fp,"(soc_id %d)",client_fd);
        fprintf(fp," client's guess = %s\n",clientGuess);
        
        printTime(fp,flag);
        printIP(fp,server);
        fprintf(fp," server's hint =[%d:%d]\n",hint[0],hint[1]);
       
    }
    if(flag==GAMEOVER_S)
    {
        printTime(fp,flag);
        printIP(fp,client);
        fprintf(fp,"(soc_id %d)",client_fd);
        fprintf(fp," SUCCESS game over\n");
        
    }
    if(flag==GAMEOVER_F)
    {
        printTime(fp,flag);
        printIP(fp,client);
        fprintf(fp,"(soc_id %d)",client_fd);
        fprintf(fp," FAILURE game over\n");
    }
    fclose(fp);


}

char* generateSecretWord()
{
    char sample[6]={'A','B','C','D','E','F'};
    char* secretWord=malloc(WORDSIZE+1);
    int r,i;
    srand(time(NULL));
    for(i=0;i<WORDSIZE;i++)
    {
    r = rand()%6;
    secretWord[i]=sample[r];
    }
    secretWord[4]='\0';
    return secretWord;

}

void printIP(FILE* fp,struct sockaddr_in client){
    fprintf(fp,"(%d.%d.%d.%d)",
              (int)(client.sin_addr.s_addr&0xFF),
              (int)((client.sin_addr.s_addr&0xFF00)>>8),
              (int)((client.sin_addr.s_addr&0xFF0000)>>16),
              (int)((client.sin_addr.s_addr&0xFF000000)>>24));
}
void printTime(FILE* fp,int flag)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    //if(flag==CONNECTION)
    fprintf(fp,"[%2d %2d %d %2d:%2d:%2d]",  tm.tm_mday, tm.tm_mon + 1,tm.tm_year + 1900, tm.tm_hour, tm.tm_min,tm.tm_sec);


}
void printStats()
{
     int i;
     fp=fopen("log.txt","a");
     fprintf(fp,"The number of client connected to the session is %d\n",numOfClient);
     fprintf(fp,"The number of client won the game is %d\n",numOfWinner);
     
     for(i=0;i<numOfClient;i++){
         //fprintf(fp,"thread %d running,stimeusage at start is %ld\n",start.ru_stime.tv_sec);
         fprintf(fp,"thread %d finished,stimeusage at end is %ld\n",usage[i].id,usage[i].start.ru_ixrss);
         
         //fprintf(fp,"thread %d running,utimeusage at start is %ld\n",usage[i].id,usage[i].start.ru_utime.tv_sec);
         fprintf(fp,"thread %d finished,utimeusage at end is %ld\n",usage[i].id,usage[i].start.ru_ixrss);
     }

     fclose(fp);
}
