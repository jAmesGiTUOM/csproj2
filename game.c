
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include "header.h"
void* game_handler(void* gameInfo)
{
       
       
       
       
       
       struct gameData info=*(struct gameData*)(gameInfo);
	     int client_fd=info.client_fd;
       int i,counter;
       int win=0;
       int n;
       int invalid;
       char secretWord[5];
       char feedBack[2];
	     char* message;
	     char word[4];
       char flag[1];
       strcpy(secretWord,info.secretWord);
	     message = "Hello,let the game begin,please enter the SecretWord\n";
             
      
       pthread_mutex_lock(&lock);
       generateLogFile(info.fp,info.client_fd,info.client,info.server,secretWord,NULL,NULL,0);
       pthread_mutex_unlock(&lock);
    	 n=write(info.client_fd , message , strlen(message));
       if(n<=0)
       {
       perror("ERROR writing to socket");
       }
       counter=0;
   	   while(!win)
       { 
              invalid=0;
              bzero(word,4);

              n=read(info.client_fd,word,4);
              if(n==0)
              {
                  return 0;
              }
              for(i=0;i<4;i++)
              {
                if(!checkValid(word[i]))
                {
                    invalid=1;
                }
              }
              if(invalid)
              {
                continue;
              }
              win=1;
              for(i=0;i<4;i++)
              {
                if(word[i]!=secretWord[i])
                {
                    win=0;
                }
              }
              bzero(flag,1);
              if(win)
              {
                flag[0]='w';
                n=write(info.client_fd,flag,1);
                 if(n<=0)
                {
                  perror("ERROR writing to socket");
                }
                pthread_mutex_lock(&lock);
                generateLogFile(info.fp,info.client_fd,info.client,info.server,secretWord,word,feedBack,GAMEOVER_S);
                pthread_mutex_unlock(&lock);
                numOfWinner++;
              }
              else if(counter==10)
              {
                flag[0]='l';
                n=write(info.client_fd,flag,1);
                 if(n<=0)
                {
                  perror("ERROR writing to socket");
                }
                pthread_mutex_lock(&lock);
                generateLogFile(info.fp,info.client_fd,info.client,info.server,secretWord,word,feedBack,GAMEOVER_F);
                pthread_mutex_unlock(&lock);
                break;
              }
              else
              {
                //bzero(feedBack,2);
                feedBack[0]=correctPlacedChar(word,secretWord);
                feedBack[1]=misplacedChar(word,secretWord);
                //printf("f0=%d\n",feedBack[0] );
                //printf("f1=%d\n",feedBack[1] );
                //printf("%s\n", word);
                n=write(info.client_fd,feedBack,sizeof(int)*2);
                 if(n<=0)
                  {
                  perror("ERROR writing to socket");
                  } 
                  word[4]='\0';
               pthread_mutex_lock(&lock);   
               generateLogFile(info.fp,info.client_fd,info.client,info.server,secretWord,word,feedBack,FEEDBACK);
               pthread_mutex_unlock(&lock);
               counter++;

              }
            }
            

            
            
}

int correctPlacedChar(char* answer,char* secretWord)
{
    int counter=0;
    int i;
    for(i=0;i<4;i++)
    {
      if(answer[i]==secretWord[i])
      {
        counter++;
      }
    }
    return counter;
}
int misplacedChar(char* answer,char* secretWord)
{
  int counter=0;
  int alist[4];
  int slist[4];
  int i,j;
    for(i=0;i<4;i++)
    {
      for(j=0;j<4;j++)
      {
        if(i==j)
        {
          continue;
        }
        if(positionChecked(slist,j,counter))
        {
          continue;
        }
        if(positionChecked(alist,i,counter))
        {
          continue;
        }
        if(answer[i]==secretWord[j])
        {
          if(answer[i]!=secretWord[i]&&answer[j]!=secretWord[j])
          {
          slist[counter]=j;
          alist[counter]=i;
          printf("i=%d,j=%d",i,j);
          counter++;
          }
        }
      }
    }
    return counter;
}
int positionChecked(int* list,int element,int num)
{
  int i;
  for(i=0;i<num;i++)
  {
    if(element==list[i])
    {
      return 1;
    }
  }
  return 0;
}
int checkValid(char c){
  if(c=='A')
  {
    return 1;
  }
  if(c=='B')
  {
    return 1;
  }
  if(c=='C')
  {
    return 1;
  }
  if(c=='D')
  {
    return 1;
  }
  if(c=='E')
  {
    return 1;
  }
  if(c=='F')
  {
    return 1;
  }

  return 0;

}

