#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include "header.h"

int main(int argc, char **argv)
{
	
			
	  pthread_mutex_lock(&lock);
	  getrusage(RUSAGE_SELF, &start);
      pthread_mutex_unlock(&lock);
	  
      int server_fd,client_fd,port_num;
      int client_length;
      int counter=0;
      int needRandom=0;
      struct sockaddr_in server,client;
      char* secretWord;
      FILE *fp;
	

      numOfClient=0;//set the initial client number to zero
	  numOfWinner=0;
      signal(SIGINT, INThandler);

      //clear the log file
      fp=fopen("log.txt","w");
      fclose(fp);
      //Create mutex lock
      if (pthread_mutex_init(&lock, NULL) != 0)
      {
      printf("\n mutex init failed\n");
      return 1;
      }


	pthread_t thread_id;

	server_fd=socket(AF_INET,SOCK_STREAM,0);
	if(server_fd<0)
	{

	printf("Socket creation failed");

	}
	   //get the port number and secret word from command line
	   port_num=atoi(argv[1]);
	   secretWord=argv[2];
              if(secretWord==NULL)
              {
                needRandom=1;
              }

		//set up the server socket address
	    server.sin_family = AF_INET;
    	server.sin_addr.s_addr = INADDR_ANY;
    	server.sin_port = htons(port_num);

    	//bind the socket
    	if(bind(server_fd,(struct sockaddr*)&server,sizeof(server))<0)
    	{
    		printf("bind failed\n");
    		exit(EXIT_FAILURE);
    	}

    	//listen to connection
    	listen(server_fd,5);
    	client_length=sizeof(client);
    	//if the return value for accept is positive,we will keep accepting clients
    	while(client_fd=accept(server_fd,(struct sockaddr*)&client,&client_length))
    	{
              
              struct gameData gameInfo;
              if(needRandom)
              {
                secretWord=generateSecretWord();
              }
              printf("%s\n",secretWord);
              strcpy(gameInfo.secretWord,secretWord);
              gameInfo.client_fd=client_fd;
			  gameInfo.server=server;
			  gameInfo.client=client;
			  gameInfo.fp=fp;
			  
			  pthread_mutex_lock(&lock);
	  		  getrusage(RUSAGE_SELF, &usage[numOfClient].start);
			  usage[numOfClient].id=client_fd;
			  numOfClient++;
              pthread_mutex_unlock(&lock);
              
    	       if( pthread_create( &thread_id , NULL ,  game_handler , (void*) &gameInfo) < 0)
        	{
            	perror("could not create thread");
            	return 1;
        	}
				
        		//pthread_join(thread_id,NULL);
    	}
    	if (client_fd< 0)
    	{
        	perror("accept failed");
        	return 1;
    	}
              printf("session ended\n");

     
    	return 0;


}

void  INThandler(int sig)
{

    signal(sig, SIG_IGN);

   	printStats();

    exit(0);     
}







