all: client server

client: client.c game.c header.h
	gcc -o client client.c game.c generateLog.c 
server: server.c game.c header.h
	gcc -o server server.c game.c generateLog.c -lpthread 

