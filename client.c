#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <unistd.h>
#include "header.h"



int main(int argc, char**argv)
{
	int client_fd, portno, n,i;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	char buffer[256];

	if (argc < 3) 
	{
		fprintf(stderr,"usage %s hostname port\n", argv[0]);
		exit(0);
	}

	portno = atoi(argv[2]);

	
	/* Translate host name into peer's IP address ;
	 * This is name translation service by the operating system 
	 */
	server = gethostbyname(argv[1]);
	
	if (server == NULL) 
	{
		fprintf(stderr,"ERROR, no such host\n");
		exit(0);
	}
	
	/* Building data structures for socket */

	bzero((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;

	bcopy((char *)server->h_addr, 
			(char *)&serv_addr.sin_addr.s_addr,
			server->h_length);

	serv_addr.sin_port = htons(portno);

	
	client_fd = socket(AF_INET, SOCK_STREAM, 0);
	
	if (client_fd < 0) 
	{
		perror("ERROR opening socket");
		exit(0);
	}
	
	if (connect(client_fd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) 
	{
		perror("ERROR connecting");
		exit(0);
	}

	/* Do processing*/
	read(client_fd,buffer,255);
	printf("%s",buffer);
	bzero(buffer,256);
	char word[10];
	int win=0;
	int inputValid;
	while(!win)
	{
		inputValid=1;

	//read the input from the user
	bzero(word,10);

	scanf("%s",&word);
	for(i=0;i<4;i++)
	{
		if(checkValid(word[i])==0)
		{
			inputValid=0;
			break;
		}
	}
	if(strlen(word)!=4)
	{
		inputValid=0;
	}
	if(!inputValid)
	{
		printf("INVALID\n");
		continue;
	}
	n = write(client_fd,word,5);
	
	
	if (n < 0) 
	{
		perror("ERROR writing to socket");
		exit(0);
	}
	
	bzero(buffer,256);

	n = read(client_fd,buffer,255);
	if (n < 0)
	{
		perror("ERROR reading from socket");
		exit(0);
	}
	if(buffer[0]=='w')
	{
	win=1;
	printf("SUCCESS\n");
	}
  	else if(buffer[0]=='l')
 	{
    printf("FAILURE\n");
		break;
  	}
	else
	{
		printf("[%d:%d]\n",buffer[0],buffer[1]);
	}
	
	
	}


	return 0;
}

